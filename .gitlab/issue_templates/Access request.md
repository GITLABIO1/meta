<!-- Title: @username access request -->

/assign USERNAME
Hi USERNAME, I'm one of the maintainers of the community forks, and it looks like you are a new contributor to GitLab :wave:

**If after reading the below you are unsure how to proceed, please come and say hello on [Discord](https://discord.gg/gitlab) (in the `#contribute` channel).**

In order to process your request, we need a bit more information.
Please introduce yourself by adding a comment to this issue (or replying to this e-mail):

* Let us know if there is an existing issue you'd like to work on (please provide a link).
* Alternatively, let us know if you have an area you'd like to contribute to (for example, backend, frontend, documentation) or would like assistance finding something to work on.
* This would be a great opportunity to update your [profile](https://gitlab.com/-/profile) with a short bio and/or links to your socials.

Once we hear back from you, we can review your request- thanks!

> NOTE:
> 
> * You can start contributing without access to the community forks (clone the repository, setup a development environment, familiarise yourself with the codebase).
> * You only need access once you are ready to push your changes.
> * You can read more about the [approval process](https://gitlab.com/gitlab-community/meta#approve-an-access-request).

Ordinarily the team/community will respond within a few hours, but feel free to follow up on [Discord](https://discord.gg/gitlab) (in the `#contribute` channel).

Thank you :slight_smile:

/due in 1 week
/assign me
/label ~access-request
